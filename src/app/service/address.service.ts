import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Address} from "../domain/address";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class AddressService {

  constructor(private httpClient: HttpClient) { }

  findAddressesByCustomerId(customerId: number): Observable<Address[]> {
    return this.httpClient.get<Address[]>(environment.apiUrl + '/customers/' + customerId + '/addresses');
  }
}
