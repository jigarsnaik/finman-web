import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Customer} from "../domain/customer";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private httpClient: HttpClient) {
  }

  findAll(): Observable<Customer[]> {
    return this.httpClient.get<Customer[]>(environment.apiUrl + '/customers');
  }

  findById(id: number): Observable<Customer> {
    return this.httpClient.get<Customer>(environment.apiUrl + '/customers/' + id);
  }

  saveOrUpdate(customer: Customer): Observable<Customer> {
    return this.httpClient.post<Customer>(environment.apiUrl + '/customers', customer, {observe: "body"});
  }
}
