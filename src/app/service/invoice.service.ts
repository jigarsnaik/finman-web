import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Invoice} from "../domain/Invoice";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";


export interface InvoiceData {
  content: Invoice[],
  empty: boolean,
  first: boolean,
  last: boolean,
  number: number,
  numberOfElements: number,
  size: number,
  totalElements: number,
  totalPages: number
}
@Injectable({
  providedIn: 'root'
})
export class InvoiceService {

  constructor(private httpClient: HttpClient) {

  }

  getAllInvoices(royaltyNo: string, invoiceNo: string, customerName: string,purchaseDateFrom: Date, purchaseDateTo: Date,page: number, size: number): Observable<InvoiceData> {
    let params = new HttpParams();
    console.log('royaltyNo : ' + royaltyNo);
    if (royaltyNo) params = params.append('royaltyNo', royaltyNo);
    if (invoiceNo) params = params.append('invoiceNo', invoiceNo);
    if (customerName) params = params.append('customerName', customerName);
    if (purchaseDateFrom) params = params.append('purchaseDateFrom', this.formatDate(purchaseDateFrom));
    if (purchaseDateTo) params = params.append('purchaseDateTo', this.formatDate(purchaseDateTo));
    params = params.append('page', page);
    params = params.append('size', size);
    return this.httpClient.get<InvoiceData>(environment.apiUrl + '/invoices', {params});
  }

  saveOrUpdate(invoice: Invoice): Observable<Invoice> {
    return this.httpClient.put<Invoice>(environment.apiUrl + '/invoices', invoice, {observe: "body"});
  }

  getNextChallanNo(): Observable<string> {
    return this.httpClient.get<string>(environment.apiUrl + '/invoices/invoiceNo/next', {observe: 'body'});
  }

  // formatString(input: string) {
  //   if (input) {
  //     return input.trim();
  //   } else {
  //     return null;
  //   }
  // }

  formatDate(date: Date): string {
    if (String(date) == 'undefined') {
      return "";
    }
    const day = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate() + '';
    const month = date.getMonth() + 1 < 10 ? `0${date.getMonth() + 1}` : (date.getMonth() + 1) + '';
    const year = date.getFullYear() + '';
    return `${day}/${month}/${year}`;
  }
}
