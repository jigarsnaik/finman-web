import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Driver} from "../domain/driver";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class DriverService {

  constructor(private httpClient: HttpClient) {
  }

  findAll(): Observable<Driver[]> {
    return this.httpClient.get<Driver[]>(environment.apiUrl + '/drivers');
  }

  saveOrUpdate(driver: Driver): Observable<Driver> {
    return this.httpClient.post<Driver>(environment.apiUrl + '/drivers', driver, {observe: "body"});
  }
}
