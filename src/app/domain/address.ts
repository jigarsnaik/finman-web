import {Customer} from "./customer";

export class Address {
  id: number;
  customer: Customer;
  site: string;
  address: string;
  city: string;
  state: string;
  country: string;
  postalCode: string;
  active: boolean

  constructor(active: boolean) {
    this.active = active;
  }
}
