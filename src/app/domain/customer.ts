import {Address} from "./address";

export class Customer {
  public id: number;
  public customerName: string;
  public mobileNo: string;
  public emailId: string;
  public addresses: Address[];
  public active: boolean

  constructor(active: boolean) {
    this.active = active;
    this.addresses = [new Address(true)];
  }

}
