export class Driver {
  id: number;
  driverName: string
  vehicleNo: string;
  mobileNo: string;
  licenseNo: string;
  active: boolean

  constructor(active: boolean) {
    this.active = active;
  }
}
