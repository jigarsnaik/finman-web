export class Product {
  id: number;
  productName: string;
  active: boolean

  constructor(active: boolean) {
    this.active = active;
  }
}
