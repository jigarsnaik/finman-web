import {Customer} from "./customer";
import {Address} from "./address";
import {Product} from "./product";
import {Driver} from "./driver";

export class Invoice {
  id: number;
  invoiceNo: string;
  purchaseDate: Date;
  royaltyNo: string;
  customer: Customer;
  address: Address;
  product: Product;
  lengthGrossWt: number;
  widthTareWt: number;
  heightNetWt: number
  totalQuantity: number;
  driver: Driver;
  outTime: string;
  active: boolean

  constructor(customer: Customer, address: Address, product: Product, driver: Driver) {
    this.customer = customer;
    this.address = address;
    this.product = product;
    this.driver = driver;
  }

}
