import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {NgForm} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Router} from "@angular/router";
import {Driver} from "../../domain/driver";
import {DriverService} from "../../service/driver.service";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-driver-form',
  templateUrl: './driver-form.component.html',
  styleUrls: ['./driver-form.component.css']
})
export class DriverFormComponent implements OnInit {

  redirect: boolean;
  driver: Driver = new Driver(true);
  @ViewChild('form') driverForm: NgForm;

  constructor(private driverService: DriverService,
              public dialogRef: MatDialogRef<DriverFormComponent>,
              private router: Router,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private snackBar: MatSnackBar) {
    console.log('DriverFormComponent')
    console.log(data)
    if (data.element) {
      this.driver = data.element;
      this.redirect = data.redirect;
    }
  }

  ngOnInit(): void {
  }

  saveDriver() {
    this.driver.active = true;
    this.driverService.saveOrUpdate(this.driver).subscribe({
      next: (next => {
        console.log('saveDriver')
        console.log(next)
        this.dialogRef.close({
          data: next
        });
        if (this.redirect) {
          this.router.navigate(['driver-list']).then(r => console.log(r));
        }
        this.snackBar.open('Driver details saved successfully', 'Info', {duration: 5000})
      }), error: (error => {
        this.snackBar.open('Oops, something went wrong while saving driver details, ' + error.message, 'Error', {duration: 30000})
      })
    })
  }

  cancel() {
    this.dialogRef.close();
  }

}
