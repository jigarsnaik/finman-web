import {Component, OnInit} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {Driver} from "../../domain/driver";
import {DriverService} from "../../service/driver.service";
import {DriverFormComponent} from "../driver-form/driver-form.component";

@Component({
  selector: 'app-driver-list',
  templateUrl: './driver-list.component.html',
  styleUrls: ['./driver-list.component.css']
})
export class DriverListComponent implements OnInit {
  dataSource: Driver[] = [];
  displayedColumns: string[] = ['driverName', 'mobileNo', 'vehicleNo', 'licenseNo', 'action'];

  constructor(private driverService: DriverService, public dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.getAllDrivers();
  }

  editDriverForm(update: string, element) {
    const dialogRef = this.dialog.open(DriverFormComponent, {
      data: {
        element: element,
        redirect: false
      },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(e => {
      this.getAllDrivers()
    })
  }

  private getAllDrivers() {
    this.driverService.findAll().subscribe(data => {
      this.dataSource = data;
      console.log(this.dataSource)
    })
  }
}
