import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatDividerModule} from '@angular/material/divider';
import {HomeComponent} from './home/home.component';
import {AboutComponent} from './about/about.component';
import {InvoiceListComponent} from './invoice/invoice-list/invoice-list.component';
import {MatTableModule} from "@angular/material/table";
import {HttpClientModule} from "@angular/common/http";
import {MatCardModule} from "@angular/material/card";
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatDialogModule} from "@angular/material/dialog";
import {MatInputModule} from "@angular/material/input";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatGridListModule} from "@angular/material/grid-list";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatNativeDateModule, MatRippleModule} from "@angular/material/core";
import {NgxMaterialTimepickerModule} from "ngx-material-timepicker";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {InvoiceFormComponent} from './invoice/invoice-form/invoice-form.component';
import {CustomerListComponent} from './customer/customer-list/customer-list.component';
import {MatChipsModule} from "@angular/material/chips";
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {MatSelectModule} from "@angular/material/select";
import {ProductListComponent} from './product/product-list/product-list.component';
import {ProductFormComponent} from './product/product-form/product-form.component';
import {DriverListComponent} from './driver/driver-list/driver-list.component';
import {DriverFormComponent} from './driver/driver-form/driver-form.component';
import {CustomerFormComponent} from './customer/customer-form/customer-form.component';
import {MatListModule} from "@angular/material/list";
import {InvoicePrintComponent} from "./invoice/invoice-print/invoice-print.component";
import {NgxExtendedPdfViewerModule} from "ngx-extended-pdf-viewer";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatSortModule} from "@angular/material/sort";
import {MatExpansionModule} from "@angular/material/expansion";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    InvoiceListComponent,
    InvoiceFormComponent,
    CustomerListComponent,
    ProductListComponent,
    ProductFormComponent,
    DriverListComponent,
    DriverFormComponent,
    CustomerFormComponent,
    InvoicePrintComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatToolbarModule,
        MatSidenavModule,
        MatButtonModule,
        MatIconModule,
        MatDividerModule,
        MatTableModule,
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        BrowserAnimationsModule,
        MatToolbarModule,
        MatIconModule,
        MatButtonModule,
        MatCardModule,
        MatProgressBarModule,
        MatSnackBarModule,
        MatDividerModule,
        MatTableModule,
        MatFormFieldModule,
        MatTooltipModule,
        MatDialogModule,
        MatInputModule,
        FormsModule,
        MatGridListModule,
        MatDatepickerModule,
        MatNativeDateModule,
        NgxMaterialTimepickerModule,
        MatSlideToggleModule,
        MatRippleModule,
        MatChipsModule,
        MatAutocompleteModule,
        MatSelectModule,
        ReactiveFormsModule,
        MatListModule,
        NgxExtendedPdfViewerModule,
        MatPaginatorModule,
        MatSortModule,
        MatExpansionModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
