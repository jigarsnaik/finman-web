import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {Address} from "../../domain/address";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {CustomerService} from "../../service/customer.service";
import {Router} from "@angular/router";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Customer} from "../../domain/customer";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-customer-form',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})
export class CustomerFormComponent implements OnInit {

  customer: Customer;
  @ViewChild('form') customerForm: NgForm;
  redirect: boolean;

  constructor(public dialogRef: MatDialogRef<CustomerFormComponent>,
              private customerService: CustomerService,
              private router: Router,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private snackBar: MatSnackBar) {
    console.log('data')
    console.log(data.element)
    if (data.element) {
      this.redirect = data.redirect;
      this.customer = data.element;
    } else {
      let customer = new Customer(true)
      customer.addresses = [new Address(true)]
      this.customer = customer;
    }
  }

  ngOnInit(): void {
  }

  saveCustomer() {
    this.customer.active = true;
    this.customerService.saveOrUpdate(this.customer).subscribe({
      next: next => {
        this.dialogRef.close({
          data: next
        })
        if (this.redirect) {
          this.router.navigate(['customer-list']).then(r => console.log(r));
        }
        this.snackBar.open('Customer saved successfully.', 'Info', {duration: 5000})
      }, error: err => {
        this.snackBar.open('Oops, something went wrong while saving customer, ' + err.message, 'Error', {duration: 30000})
      }

    });
  }

  saveCustomer_add() {
    this.customerService.saveOrUpdate(this.customer).subscribe({
      next: (customer => {
        console.log('customerService.saveOrUpdate Response.')
        console.log(customer)
        this.dialogRef.close({
          data: customer
        })
        if (this.redirect) {
          this.router.navigate(['customer-list']).then(r => console.log(r));
        }
        this.snackBar.open('Customer saved successfully.', 'Info', {duration: 5000})
      }),
      error: (err => {
        this.snackBar.open('Oops, something went wrong while saving customer. ' + err.error, 'Error', {duration: 30000})
      })
    });
  }

  cancel() {
    this.dialogRef.close()
  }

  addAddress() {
    this.customer.addresses.push(new Address(true))
  }

  removeAddress() {
    this.customer.addresses.pop();
  }

}
