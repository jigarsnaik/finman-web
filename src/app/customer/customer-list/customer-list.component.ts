import {Component, OnInit} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {Customer} from "../../domain/customer";
import {CustomerService} from "../../service/customer.service";
import {MatTableDataSource} from "@angular/material/table";
import {MatDialog} from "@angular/material/dialog";
import {Element} from "@angular/compiler";
import {CustomerFormComponent} from "../customer-form/customer-form.component";
import {Address} from "../../domain/address";

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', visibility: 'hidden'})),
      state('expanded', style({height: '*', visibility: 'visible'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class CustomerListComponent implements OnInit {
  customers: any[] = [];
  displayedColumns = ['customerName', 'mobileNo', 'emailId', 'action'];
  dataSource = new MatTableDataSource<Customer>([]);
  expandedElement: any;

  constructor(private customerService: CustomerService, public dialog: MatDialog) {

  }

  isExpansionDetailRow = (i: number, row: Object) => row.hasOwnProperty('detailRow');

  ngOnInit(): void {
    this.getAllCustomers();
  }

  getAllCustomers() {
    this.customerService.findAll().subscribe(data => {
      // @ts-ignore
      this.customers = data;
      // @ts-ignore
      const rows = [];
      // @ts-ignore
      this.customers.forEach(element => rows.push(element, {detailRow: true, element}));
      // @ts-ignore
      this.dataSource.data = rows;

    })
  }

  addNewCustomerOpenDialog() {

    let customer = new Customer(true);
    customer.addresses = [new Address(true)];

    const dialogRef = this.dialog.open(CustomerFormComponent, {
      data: {
        redirect: true,
        element: customer
      },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(e => {
      this.getAllCustomers()
    })
  }


  openDialogEditCustomer(update: string, element: Element) {
    let data = {
      element: element,
      redirect: true
    }
    const dialogRef = this.dialog.open(CustomerFormComponent, {
      data: data
    });
    dialogRef.afterClosed().subscribe(e => {
      this.getAllCustomers()
    })
  }

}
