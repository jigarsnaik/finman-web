import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {AboutComponent} from "./about/about.component";
import {InvoiceListComponent} from "./invoice/invoice-list/invoice-list.component";
import {CustomerListComponent} from "./customer/customer-list/customer-list.component";
import {ProductListComponent} from "./product/product-list/product-list.component";
import {DriverListComponent} from "./driver/driver-list/driver-list.component";

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'invoice-list', component: InvoiceListComponent},
  {path: 'customer-list', component: CustomerListComponent},
  {path: 'product-list', component: ProductListComponent},
  {path: 'driver-list', component: DriverListComponent},
  {path: 'home', component: HomeComponent},
  {path: 'about', component: AboutComponent},
  {path: '**', component: HomeComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
