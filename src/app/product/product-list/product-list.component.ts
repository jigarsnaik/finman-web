import {Component, OnInit} from '@angular/core';
import {Product} from "../../domain/product";
import {MatDialog} from "@angular/material/dialog";
import {ProductFormComponent} from "../product-form/product-form.component";
import {ProductService} from "../../service/product.service";

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  dataSource: Product[] = [];
  displayedColumns: string[] = ['productName', 'action'];

  constructor(private productService: ProductService, public dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.getAllProducts();
  }

  openProductForm(update: string, element) {
    const dialogRef = this.dialog.open(ProductFormComponent, {
      data: {
        element: element,
        redirect: false
      },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(e => {
      this.getAllProducts()
    })
  }

  private getAllProducts() {
    this.productService.findAll().subscribe(data => {
      this.dataSource = data;
      console.log(this.dataSource)
    })
  }
}
