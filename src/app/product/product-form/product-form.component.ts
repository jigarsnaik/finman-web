import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {NgForm} from "@angular/forms";
import {Product} from "../../domain/product";
import {ProductService} from "../../service/product.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Router} from "@angular/router";
import {HttpErrorResponse} from "@angular/common/http";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {

  product: Product = new Product(true);
  redirect: boolean;

  @ViewChild('form') productForm: NgForm;

  constructor(private productService: ProductService,
              public dialogRef: MatDialogRef<ProductFormComponent>,
              private router: Router,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private snackBar: MatSnackBar) {
    this.redirect = data.redirect;
    this.product = {...data.element};
  }

  ngOnInit(): void {
  }

  saveProduct() {
    this.product.active = true;
    this.productService.saveOrUpdate(this.product).subscribe({
      next: (product) => {
        this.dialogRef.close({
          data: product
        })
        if (this.redirect) {
          this.router.navigate(['product-list']);
        }
        this.snackBar.open('Material details saved successfully.','Info', {duration: 5000})
      },
      error: (err => {
        this.snackBar.open('Error while saving product, please contact admin' + err.error, 'Error', {duration: 30000})
      })
    });
  }

  cancel() {
    this.dialogRef.close();
  }
}
