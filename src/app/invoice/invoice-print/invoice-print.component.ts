import {Component, Inject} from '@angular/core';
import {pdfDefaultOptions} from 'ngx-extended-pdf-viewer';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-invoice-print',
  templateUrl: './invoice-print.component.html',
  styleUrls: ['./invoice-print.component.css']
})
export class InvoicePrintComponent {

  src!: string

  constructor(public dialogRef: MatDialogRef<InvoicePrintComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    console.log('invoice print')
    console.log(data)
    this.src = environment.apiUrl + '/invoices/' + data.id + '/download'
    pdfDefaultOptions.assetsFolder = 'bleeding-edge';
  }


}
