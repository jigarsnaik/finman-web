import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl, NgForm, Validators} from "@angular/forms";
import {Invoice} from "../../domain/Invoice";
import {InvoiceService} from "../../service/invoice.service";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {Router} from "@angular/router";
import {map, Observable, startWith} from "rxjs";
import {Customer} from "../../domain/customer";
import {CustomerService} from "../../service/customer.service";
import {Address} from "../../domain/address";
import {MatAutocompleteSelectedEvent} from "@angular/material/autocomplete";
import {Product} from "../../domain/product";
import {ProductService} from "../../service/product.service";
import {ProductFormComponent} from "../../product/product-form/product-form.component";
import {Driver} from "../../domain/driver";
import {DriverService} from "../../service/driver.service";
import {DriverFormComponent} from "../../driver/driver-form/driver-form.component";
import {AddressService} from "../../service/address.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {CustomerFormComponent} from "../../customer/customer-form/customer-form.component";
import {InvoicePrintComponent} from "../invoice-print/invoice-print.component";

@Component({
  selector: 'app-invoice-add',
  templateUrl: './invoice-form.component.html',
  styleUrls: ['./invoice-form.component.css']
})
export class InvoiceFormComponent implements OnInit {

  invoice: Invoice;
  @ViewChild('form') invoiceForm: NgForm;
  insertMode: boolean;

  customerNameAutoComplete: FormControl = new FormControl('', [Validators.required]);
  addressAutoComplete: FormControl = new FormControl('', [Validators.required]);
  productNameAutoComplete: FormControl = new FormControl('', [Validators.required]);
  driverNameAutoComplete: FormControl = new FormControl('', [Validators.required]);

  filteredCustomers: Observable<any[]>;
  filteredAddresses: Observable<any[]>;
  filteredProducts: Observable<any[]>;
  filteredDrivers: Observable<any[]>;

  customers: Customer[] = [];
  addresses: Address[] = [];
  products: Product[] = [];
  drivers: Driver[] = [];

  constructor(public dialogRef: MatDialogRef<InvoiceFormComponent>,
              private invoiceService: InvoiceService,
              private customerService: CustomerService,
              private productService: ProductService,
              private driverService: DriverService,
              private addressService: AddressService,
              private router: Router,
              @Inject(MAT_DIALOG_DATA) public data: Invoice,
              public dialog: MatDialog,
              private snackBar: MatSnackBar) {
    this.insertMode = data == null;
    this.invoice = {...data};
  }

  ngOnInit(): void {
    this.loadCustomerList();
    this.loadProducts();
    this.loadDrivers();

    if (this.insertMode) {
      this.invoice = new Invoice(new Customer(true), new Address(true), new Product(true), new Driver(true));
      const isoDate = new Date(this.invoice.purchaseDate);
      this.invoice.purchaseDate = new Date(isoDate.getFullYear(), isoDate.getMonth(), isoDate.getDate(), isoDate.getHours(), isoDate.getMinutes() - isoDate.getTimezoneOffset());
      this.invoiceService.getNextChallanNo().subscribe({
        next: (next => {
          this.invoice.invoiceNo = next;
          this.invoice.active = true;
          this.invoice.purchaseDate = new Date();
        }),
        error: (err => {
          this.snackBar.open('Error while generating challan no. ' + err.message())
        })
      })
    } else {
      this.invoice.outTime = this.invoice.outTime.substr(0, 5);
    }
  }

  saveInvoice() {
    const isoDate = new Date(this.invoice.purchaseDate)
    this.invoice.purchaseDate = new Date(isoDate.getFullYear(), isoDate.getMonth(), isoDate.getDate(), isoDate.getHours(), isoDate.getMinutes() - isoDate.getTimezoneOffset());
    console.log('save invoice')
    console.log(this.invoice)
    this.invoiceService.saveOrUpdate(this.invoice).subscribe({
      next: (next => {
        this.dialogRef.close()
        this.router.navigate(['invoice-list']);
        this.snackBar.open('Challan saved successfully.', 'Info', {
          duration: 5000
        })
      }),
      error: (err => {
        this.snackBar.open('Error while saving challan. Please contract admin. ' + err.message, 'Error', {
          duration: 30000
        })
      })

    });
  }

  saveInvoiceAndPrint() {
    const isoDate = new Date(this.invoice.purchaseDate)
    this.invoice.purchaseDate = new Date(isoDate.getFullYear(), isoDate.getMonth(), isoDate.getDate(), isoDate.getHours(), isoDate.getMinutes() - isoDate.getTimezoneOffset());
    console.log('save invoice')
    console.log(this.invoice)
    this.invoiceService.saveOrUpdate(this.invoice).subscribe({
      next: (next => {
        const dialogRef = this.dialog.open(InvoicePrintComponent, {
          data: next,
          width: '100%',
        });
        dialogRef.afterClosed().subscribe(e => {
          this.dialogRef.close()
          this.snackBar.open('Challan saved successfully.', 'Info', {
            duration: 5000
          })
        })
      }),
      error: (err => {
        this.snackBar.open('Error while saving challan. Please contract admin. ' + err.message, 'Error', {
          duration: 30000
        })
      })

    });
  }

  cancel() {
    this.dialogRef.close()
  }

  filterCustomer(name: string) {
    return this.customers.filter(customer => customer.customerName.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }

  filterAddress(name: string) {
    return this.addresses.filter(address => (address.address.toLowerCase().indexOf(name.toLowerCase())) === 0);
  }

  filterProduct(name: string) {
    return this.products.filter(product => (product.productName.toLowerCase().indexOf(name.toLowerCase())) === 0);
  }

  filterDriver(name: string) {
    return this.drivers.filter(driver => (driver.driverName.toLowerCase().indexOf(name.toLowerCase())) === 0);
  }

  updateCustomerDetails(event: any) {
    if (event.option.id == -1) {
      this.showCustomerFormDialog()
    } else {
      let selectedCustomer = this.customers.filter(customer => customer.id == event.option.id).pop();
      this.addressAutoComplete.setValue(null)

      this.invoice.customer.id = selectedCustomer.id;
      this.invoice.customer.mobileNo = selectedCustomer.mobileNo;
      this.invoice.customer.emailId = selectedCustomer.emailId
      this.invoice.customer.customerName = selectedCustomer.customerName;
      this.addresses = selectedCustomer.addresses;
      this.filteredAddresses = this.addressAutoComplete.valueChanges
        .pipe(
          startWith(''),
          map(address => address ? this.filterAddress(address) : this.addresses.slice())
        );
    }
  }

  updateAddressDetails(event: any) {
    if (event.option.id == -1) {
      this.showAddressFormDialog()
    } else {
      let selectedAddress = this.addresses.filter(address => address.id == event.option.id).pop();
      this.invoice.address.id = selectedAddress.id;
      this.invoice.address.address = selectedAddress.address;
      this.invoice.address.city = selectedAddress.city;
      this.invoice.address.city = selectedAddress.state;
      this.invoice.address.state = selectedAddress.postalCode;
    }
  }

  updateProductDetail(event: any) {
    if (event.option.id == -1) {
      this.showProductFormDialog()
    } else {
      let selectedProduct = this.products.filter(product => product.id == event.option.id).pop();
      console.log(selectedProduct)
      this.invoice.product.id = selectedProduct.id;
      this.invoice.product.productName = selectedProduct.productName;
    }
  }

  updateDriverDetail(event: any) {
    if (event.option.id == -1) {
      this.showDriverFormDialog()
    } else {
      let selectedDrivers = this.drivers.filter(driver => driver.id == event.option.id).pop();
      this.invoice.driver.id = selectedDrivers.id;
      this.invoice.driver.driverName = selectedDrivers.driverName;
      this.invoice.driver.vehicleNo = selectedDrivers.vehicleNo;
      this.invoice.driver.mobileNo = selectedDrivers.mobileNo;
      this.invoice.driver.licenseNo = selectedDrivers.licenseNo;
    }
  }

  showCustomerFormDialog() {
    const dialogRef = this.dialog.open(CustomerFormComponent, {
      data: {redirect: false},
      disableClose: true
    })
    dialogRef.afterClosed().subscribe({
      next: (next => {
        this.invoice.customer.id = next.data.id;
        this.customerNameAutoComplete.setValue(next.data.customerName)
        this.loadCustomerList()
      }),
      error: (err => {
        this.snackBar.open('Error while saving customer, please try again.' + err.message, 'Error', {duration: 30000})
      })
    });

  }

  showAddressFormDialog() {
    this.customerService.findById(this.invoice.customer.id).subscribe(res => {
      console.log('debug')
      console.log(res)
      this.invoice.customer = res;
      const dialogRef = this.dialog.open(CustomerFormComponent, {
        data: {
          element: this.invoice.customer,
          redirect: false
        },
        disableClose: true
      })

      dialogRef.afterClosed().subscribe(res => {
        this.loadCustomerList();
      })
    });
  }

  showProductFormDialog() {
    const dialogRef = this.dialog.open(ProductFormComponent, {
      data: {
        element: new Product(true),
        redirect: false
      },
      disableClose: true
    })

    dialogRef.afterClosed().subscribe({
      next: (next => {
        this.loadProducts();
        this.invoice.product.id = next.data.id;
        this.productNameAutoComplete.setValue(next.data.productName)
      }),
      error: (err => {
        this.snackBar.open('Error while creating product, please contract admin. ' + err.message)
      })
    })
  }

  showDriverFormDialog() {
    const dialogRef = this.dialog.open(DriverFormComponent, {
      data: {
        element: new Driver(true),
        redirect: false
      },
      disableClose: true
    })

    dialogRef.afterClosed().subscribe({
      next: (next => {
        this.loadDrivers();
        this.invoice.driver.id = next.data.id
        this.driverNameAutoComplete.setValue(next.data.driverName)
      }),
      error: (error => {
        this.snackBar.open('Oops, something went wrong while saving driver details, ' + error.message, 'Error', {duration: 30000})
      })
    })
  }

  private loadCustomerList() {
    this.customerService.findAll().subscribe(data => {
      this.customers = data
      this.filteredCustomers = this.customerNameAutoComplete.valueChanges
        .pipe(
          startWith(''),
          map(customer => customer ? this.filterCustomer(customer) : this.customers.slice())
        );

      if (this.invoice.customer.id) {
        const event: MatAutocompleteSelectedEvent = {
          option: {
            id: this.invoice.customer.id + '',
            value: this.invoice.customer.customerName
          }
        } as MatAutocompleteSelectedEvent;
        this.updateCustomerDetails(event)
        this.customerNameAutoComplete.setValue(this.invoice.customer.customerName);
        this.addressAutoComplete.setValue(this.invoice.address.address);
      }
    });
  }

  private loadProducts() {
    this.productService.findAll().subscribe(data => {
      this.products = data
      this.filteredProducts = this.productNameAutoComplete.valueChanges
        .pipe(
          startWith(''),
          map(product => product ? this.filterProduct(product) : this.products.slice())
        );

      if (this.invoice.product.id) {
        const event: MatAutocompleteSelectedEvent = {
          option: {
            id: this.invoice.product.id + '',
            value: this.invoice.product.productName
          }
        } as MatAutocompleteSelectedEvent;
        this.updateProductDetail(event)
        this.productNameAutoComplete.setValue(this.invoice.product.productName);
      }
    });
  }

  private loadDrivers() {
    this.driverService.findAll().subscribe(data => {
      this.drivers = data
      this.filteredDrivers = this.driverNameAutoComplete.valueChanges
        .pipe(
          startWith(''),
          map(driver => driver ? this.filterDriver(driver) : this.drivers.slice())
        );

      if (this.invoice.driver.id) {
        const event: MatAutocompleteSelectedEvent = {
          option: {
            id: this.invoice.driver.id + '',
            value: this.invoice.driver.driverName
          }
        } as MatAutocompleteSelectedEvent;
        this.updateDriverDetail(event)
        this.driverNameAutoComplete.setValue(this.invoice.driver.driverName);
      }
    });
  }
}

