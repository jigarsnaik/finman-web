import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {InvoiceService} from "../../service/invoice.service";
import {Element} from "@angular/compiler";
import {Invoice} from "../../domain/Invoice";
import {MatDialog} from "@angular/material/dialog";
import {InvoiceFormComponent} from "../invoice-form/invoice-form.component";
import {InvoicePrintComponent} from "../invoice-print/invoice-print.component";
import {MatTableDataSource} from "@angular/material/table";
import {PageEvent} from "@angular/material/paginator";

export class InvoiceSearchForm {
  invoiceNo: string
  royaltyNo: string
  customerName: string
  purchaseDateFrom: Date
  purchaseDateTo: Date
}

@Component({
  selector: 'app-invoice-list',
  templateUrl: './invoice-list.component.html',
  styleUrls: ['./invoice-list.component.scss']
})
export class InvoiceListComponent implements OnInit {

  dataSource: MatTableDataSource<Invoice>;
  defaultPageSize: number;
  pageSize: number;
  page: number;
  totalCount: number;
  invoiceSearchForm: InvoiceSearchForm;


  @ViewChild('table') table: ElementRef;

  displayedColumns: string[] = ['invoiceNo', 'purchaseDate', 'royaltyNo', 'customerName'
    , 'address', 'mobileNo',
    'material', 'lengthGrossWt', 'widthTareWt', 'heightNetWt', 'totalQuantity', 'driverName', 'vehicleNo', 'outTime', 'action'
  ];

  constructor(private invoiceService: InvoiceService, public dialog: MatDialog) {
    this.invoiceSearchForm = new InvoiceSearchForm();
  }

  ngOnInit(): void {
    this.defaultPageSize = 10;
    this.page = 0;
    this.pageSize = this.defaultPageSize;
    this.dataSource = new MatTableDataSource<Invoice>();
    this.getAllInvoices(this.page, this.pageSize)
  }

  getAllInvoices(page: number, pageSize: number) {
    this.invoiceService.getAllInvoices(this.invoiceSearchForm.royaltyNo, this.invoiceSearchForm.invoiceNo, this.invoiceSearchForm.customerName, this.invoiceSearchForm.purchaseDateFrom, this.invoiceSearchForm.purchaseDateTo, page, pageSize).subscribe(data => {
      console.log(data)
      this.dataSource = new MatTableDataSource<Invoice>(data.content);
      this.pageSize = data.size;
      this.totalCount = data.totalElements;
      this.page = data.number;
    })
  }

  onPaginateChange(event: PageEvent) {
    this.getAllInvoices(event.pageIndex, event.pageSize);
  }

  openUpdateInvoiceDialog(update: string, element: Element) {
    const dialogRef = this.dialog.open(InvoiceFormComponent, {
      data: element,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(e => {
      this.getAllInvoices(this.page, this.pageSize)
    })
  }

  openAddNewInvoiceDialog() {
    const dialogRef = this.dialog.open(InvoiceFormComponent, {disableClose: true});
    dialogRef.afterClosed().subscribe(e => {
      this.getAllInvoices(this.page, this.pageSize)
    })
  }

  openInvoicePrintDialog(element) {
    const dialogRef = this.dialog.open(InvoicePrintComponent, {
      data: element,
      disableClose: false,
      width: '100%',
    });
    dialogRef.afterClosed().subscribe(e => {
      this.getAllInvoices(this.page, this.pageSize)
    })
  }

  search() {
    this.getAllInvoices(this.page, this.pageSize)
  }

  reset() {
    this.invoiceSearchForm = new InvoiceSearchForm();
    this.getAllInvoices(0, this.defaultPageSize);
  }
}

